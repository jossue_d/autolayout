//
//  MemoryViewController.swift
//  autoLayout
//
//  Created by Jossué Dután on 10/7/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class MemoryViewController: UIViewController {

    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var messageTextField: UITextField!
    
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
        messageLabel.text = messageTextField.text
        UserDefaults.standard.set(messageTextField.text, forKey: "message") //recuperar el valor que está almacenado, se puede guardar datos primitivos
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let message:String? = UserDefaults.standard.object(forKey: "message") as? String
        messageLabel.text = message
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
