//
//  TapViewController.swift
//  autoLayout
//
//  Created by Jossué Dután on 4/7/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var UIView1: UIView!
    @IBOutlet weak var UIView2: UIView!
    @IBOutlet weak var UIView3: UIView!
    @IBOutlet weak var UIView4: UIView!
    
    @IBAction func Tap1Gesture(_ sender: UITapGestureRecognizer) {
        UIView1.backgroundColor = .black
    }
    
    @IBAction func Tap2Gesture(_ sender: Any) {
        UIView2.backgroundColor = .black
    }
    
    @IBAction func Tap3Gesture(_ sender: Any) {
        UIView3.backgroundColor = .black
    }
    
    @IBAction func Tap4Gesture(_ sender: Any) {
        UIView4.backgroundColor = .black
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
