//
//  RotationViewController.swift
//  autoLayout
//
//  Created by Jossué Dután on 3/7/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class RotationViewController: UIViewController {

    @IBAction func rotationGestureActivated(_ sender: UIRotationGestureRecognizer) {
        guard sender.view != nil else { return }
        
        if sender.state == .began || sender.state == .changed {
            sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
        if sender.state == .ended{
            UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .curveEaseOut , animations: {
                sender.view?.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
            }, completion: { _ in
                sender.rotation = 0
            })
        }
 
    }
    @IBOutlet weak var viewC: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
