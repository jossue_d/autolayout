//
//  PinchViewController.swift
//  autoLayout
//
//  Created by Jossué Dután on 27/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {
    
    @IBOutlet weak var viewCon: UIView!
    @IBOutlet weak var heightC: NSLayoutConstraint!
    
    @IBAction func longPressActivated(_ sender: Any) {
        self.viewCon.backgroundColor = .red
    }
    
    
    @IBOutlet weak var widthC: NSLayoutConstraint!
    
    var originalHeight:CGFloat = 0 //CGFloat es para coordenadas y planos
    var originalWidth:CGFloat = 0
    
    @IBAction func pinchGestureActivated(_ sender: UIPinchGestureRecognizer) {
        
        //let pinch = sender as! UIPinchGestureRecognizer
        print("scale: \(sender.scale)")
        print("velocity: \(sender.velocity)")
        
        heightC.constant = originalHeight * sender.scale
        widthC.constant = originalWidth * sender.scale
        
        if (sender.state == .ended){
            
            UIView.animate(withDuration: 0.3,
                           animations: {
                            self.viewCon.alpha = 1
            },
                           completion: { finished in
                            self.heightC.constant = self.originalHeight
                            self.widthC.constant = self.originalHeight
            })
           
            
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalWidth = widthC.constant
        originalHeight = heightC.constant
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
